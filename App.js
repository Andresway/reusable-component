import React from "react";
import _ from "lodash";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  processColor,
  TouchableOpacity,
  Animated
} from "react-native";

import { BarChart } from "react-native-charts-wrapper";

var data = require("./data");

Array.prototype.insert = function(index, item) {
  this.splice(index, 0, item);
};

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      dataAudienceAgeGender: null,
      maleAudienceWidth: 0,
      femaleAudienceWidth: 0,
      maleAudienceOutput: 0,
      femaleAudienceOutput: 0,
      maleAudienceTotal: 0,
      femaleAudienceTotal: 0,
      //Animation related
      fadeAnim: new Animated.Value(0),

      legend: {
        enabled: true,
        textSize: 12,
        form: "SQUARE",
        formSize: 10,
        xEntrySpace: 20,
        yEntrySpace: 20,
        formToTextSpace: 20,

        wordWrapEnabled: true,
        maxSizePercent: 0.5
      },
      data: {},

      xAxis: {
        valueFormatter: [
          "13-17",
          "18-24",
          "25-34",
          "35-44",
          "45-54",
          "55-64",
          "65+"
        ],
        granularityEnabled: true,
        granularity: 1,

        drawLabels: true,
        position: "BOTTOM",
        drawAxisLine: true,
        drawGridLines: false,
        fontFamily: "HelveticaNeue-Medium",

        textSize: 8,
        textColor: processColor("black")
      },
      yAxis: {
        left: {
          axisLineColor: processColor("black"),
          gridColor: processColor("transparent"),
          enabled: false
        },
        right: {
          enabled: false,
          drawLabels: true,
          drawAxisLine: false,
          axisLineColor: processColor("red"),
          drawGridLines: false
        }
      }
    };
  }

  UNSAFE_componentWillMount() {
    this.getAudienceAge(data.audience.genderAge, "all");
  }

  componentDidMount() {
    Animated.timing(this.state.fadeAnim, {
      toValue: 307,
      duration: 1000
    }).start();
  }

  getAudienceAge(audienceAge, state) {
    var genderAgeStructure = [
      { name: "F.13-17" },
      { name: "F.18-24" },
      { name: "F.25-34" },
      { name: "F.35-44" },
      { name: "F.45-54" },
      { name: "F.55-64" },
      { name: "F.65+" },
      { name: "M.13-17" },
      { name: "M.18-24" },
      { name: "M.25-34" },
      { name: "M.35-44" },
      { name: "M.45-54" },
      { name: "M.55-64" },
      { name: "M.65+" },
      { name: "U.18-24" },
      { name: "U.25-34" },
      { name: "U.35-44" },
      { name: "U.45-54" },
      { name: "U.55-64" },
      {
        name: "U.65+"
      }
    ];

    genderAgeStructure.map((s, i) => {
      if (!_.find(audienceAge, { name: s.name }))
        audienceAge.insert(i, { name: s.name, value: 0 });
    });

    // Segmented Arrays
    var femaleAudienceAge = [];
    var maleAudienceAge = [];
    var allAudienceAge = [];

    // Totals
    var totalAudience = 0;
    var totalMaleAudience = 0;
    var totalFemaleAudience = 0;

    // Dynamic array that shows the data
    var data = []; // state
    if (audienceAge)
      audienceAge.map((item, index) => {
        if (item.name.substring(0, 1) == "F") {
          femaleAudienceAge.push(item.value);

          totalFemaleAudience = totalFemaleAudience + item.value;
        } else if (item.name.substring(0, 1) == "M") {
          maleAudienceAge.push(item.value);

          totalMaleAudience = totalMaleAudience + item.value;
        }
      });
    if (femaleAudienceAge)
      // Array with all the information
      femaleAudienceAge.map((item, index) => {
        allAudienceAge.push(maleAudienceAge[index] + item);
      });

    // Get all the audience
    totalAudience = totalMaleAudience + totalFemaleAudience;

    switch (state) {
      case "all":
        data = allAudienceAge;
        audienceAgeGraphColor = "#6875E2";
        break;

      case "male":
        data = maleAudienceAge;

        audienceAgeGraphColor = "#448AFF";
        break;

      case "female":
        data = femaleAudienceAge;
        audienceAgeGraphColor = "#FF97C2";
        break;

      default:
    }

    this.setState(
      {
        dataAudienceAgeGender: data,
        maleAudienceWidth: (totalMaleAudience / totalAudience) * 100 + "%",
        femaleAudienceWidth: (totalFemaleAudience / totalAudience) * 100 + "%",
        femaleAudienceTotal: femaleAudienceAge,
        maleAudienceTotal: maleAudienceAge,
        femaleAudienceOutput: Math.round(
          (totalFemaleAudience / totalAudience) * 100
        ),
        maleAudienceOutput: Math.round(
          (totalMaleAudience / totalAudience) * 100
        )
      },
      () => {
        this.setState({
          ...this.state,
          data: {
            dataSets: [
              {
                values: this.state.dataAudienceAgeGender,
                label: "All audience Gender",
                config: {
                  color: processColor("#6875E2"),

                  drawLabels: false,
                  drawValues: true,
                  drawAxisLine: false
                }
              }
            ]
          }
        });
      }
    );
  }

  getAudienceGenderRatio() {
    return (
      <View
        style={{
          maxWidth: Platform.OS === "android" ? "90%" : 318,
          marginTop: 24
        }}
      >
        {this.state.femaleAudienceWidth !== null && (
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Animated.View
              style={{
                height: 30,
                width: this.state.fadeAnim,
                backgroundColor: "#FF97C2",
                borderRadius: 2,
                maxWidth: this.state.femaleAudienceWidth
              }}
            />

            <Text style={[{ color: "#404B69", fontSize: 11, marginLeft: 15 }]}>
              {this.state.femaleAudienceOutput} %
            </Text>
          </View>
        )}

        {this.state.maleAudienceWidth !== null && (
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 15
            }}
          >
            <Animated.View
              style={{
                height: 30,
                width: this.state.fadeAnim,
                backgroundColor: "#448AFF",
                borderRadius: 2,
                maxWidth: this.state.maleAudienceWidth
              }}
            />
            <Text
              style={[
                {
                  color: "#404B69",
                  fontSize: 11,
                  borderRadius: 2,
                  marginLeft: 15
                }
              ]}
            >
              {this.state.maleAudienceOutput} %
            </Text>
          </View>
        )}
      </View>
    );
  }

  render() {
    return (
      <View style={{ height: 200, marginTop: 100 }}>
        {this.state.dataAudienceAgeGender != "" &&
          this.state.dataAudienceAgeGender != null && (
            <View style={{ marginLeft: 24 }}>
              <View style={{ marginRight: 24, justifyContent: "center" }}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between"
                  }}
                >
                  <Text style={[{ fontSize: 18 }]}>Audience Age</Text>

                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          ...this.state,

                          data: {
                            dataSets: [
                              {
                                values: this.state.dataAudienceAgeGender,
                                label: "All audience Gender",
                                config: {
                                  color: processColor("#6875E2"),

                                  drawLabels: false,
                                  drawValues: true,
                                  drawAxisLine: false
                                }
                              }
                            ]
                          }
                        })
                      }
                    >
                      <Text style={[{ fontSize: 14 }]}>All</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{ marginLeft: 20 }}
                      onPress={() =>
                        this.setState({
                          ...this.state,

                          data: {
                            dataSets: [
                              {
                                values: this.state.femaleAudienceTotal,
                                label: "Female Audience",
                                config: {
                                  color: processColor("#FF97C2"),

                                  drawLabels: false,
                                  drawValues: true,
                                  drawAxisLine: false
                                }
                              }
                            ]
                          }
                        })
                      }
                    >
                      <Text style={[{ fontSize: 14 }]}>Female</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{ marginLeft: 20 }}
                      onPress={() =>
                        this.setState({
                          ...this.state,
                          data: {
                            dataSets: [
                              {
                                values: this.state.maleAudienceTotal,
                                label: "Male audience Gender",
                                config: {
                                  color: processColor("#448AFF"),

                                  drawLabels: false,
                                  drawValues: true,
                                  drawAxisLine: false
                                }
                              }
                            ]
                          }
                        })
                      }
                    >
                      <Text style={[{ fontSize: 14 }]}>Male</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                {/* {this.getAudienceAge(history.insights.audience.genderAge)} */}

                <View style={{ width: 318, justifyContent: "center" }}>
                  {this.state.dataAudienceAgeGender && (
                    <View style={styles.container}>
                      <BarChart
                        style={styles.chart}
                        data={this.state.data}
                        xAxis={this.state.xAxis}
                        yAxis={this.state.yAxis}
                        chartDescription={{ text: "" }}
                        scaleEnabled={false}
                        touchEnabled={false}
                        /* drawGridBackground={false} */
                        animation={{
                          random: Math.random(),
                          durationY: 500
                        }}
                        extraOffsets={{ bottom: 10 }}
                        legend={this.state.legend}
                        /* gridBackgroundColor={processColor("#ffffff")} */
                        /*   visibleRange={{ x: { min: 5, max: 5 } }} */
                        /*  drawBarShadow={false} */
                        drawValueAboveBar={true}

                        /*   drawHighlightArrow={true} */
                        /*   onSelect={this.handleSelect.bind(this)} */
                        /*     highlights={this.state.highlights} */
                        /*   onChange={event => console.warn(event.nativeEvent)} */
                      />
                    </View>
                  )}
                </View>

                <View
                  style={{
                    marginRight: 24,
                    width: 318,
                    justifyContent: "center",

                    marginTop: 20
                  }}
                >
                  <Text style={[{ fontSize: 18 }]}>Audience Gender</Text>

                  {this.getAudienceGenderRatio()}
                </View>
              </View>
            </View>
          )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 327,
    height: 200,
    backgroundColor: "transparent"
  },
  chart: {
    width: 327,
    height: 200
  }
});

export default App;
